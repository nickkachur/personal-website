# Notes

This page is intended to host a little bit of microblog style content,
think short little snippets of what's on the brain at any given moment.

**Table of Contents**

[[toc]]


## Twenty-Six Years

_Posted 2020-06-21._

I haven't been much for reflection in my life. These past few years of
milestones, 18 years, 21 years, 25 years, haven't seen much explicit
celebration or made too much of a mark, but considering I am already
well past what could charitably be called a quarter of my lifespan, it
seems like a good time to reflect.

Twenty-six years. I am twenty-six years old, and what do I have to show
for all that time? What do I have to look forward to? I think it would
be fair to say that there are some things I regret about my life and
its phases, but I am grateful to be here at all, and grateful for all
the opportunities I've had. I'm even perversely grateful for the
setbacks, as they've shown my resilience and the love of those around
me. Thank God.

And thank you for reading, whether you're me in the future, my parents,
friends, or a stranger. I'd like to leave you with one small exhortation
that I'm trying to keep in mind for myself:

Stay true.

_-Nicholas_

[Back to Top](#notes)


## Better Than Before

_Posted 2020-04-30._

One of my favorite YouTubers has put out a video that I find to be
fantastic called
[Lockdown Productivity: Spaceship You](https://youtu.be/snAhsXyO3Ck).
In yet another one of his semi-contrived thought experiments CGPGrey
gives us the admonition to return from lockdown better than we were
before.

Becoming better than I was before is something I've long desired, but
still struggle to do today. It seems very hard to commit to some
enormous plan of action, so what I'm doing now is a trying a couple
small things at a time. It doesn't always work, these new mini-habits
don't always stick, but the yearning for personal growth is there, and I
hope that's enough.

_-Nicholas_

[Back to Top](./#notes)


## Lights in the Darkness

_Posted 2020-04-11._

So, there's a lot going on in the world right now, some of which seems a
little hard to handle (I'm looking at you global pandemic :eyes:), but
there's always light in the darkness if you know where to look. Some of
the more obvious encouragement I'm seeing come from places like the
majority of the hierarchs of the Orthodox Church, who have largely taken
this seriously (see the
[Assembly of Bishops Statement](http://www.assemblyofbishops.org/news/2020/pascha-2020)
among others). I'm also encouraged by a number of my favorite musicians
playing for the world, particularly Barenaked Ladies with their
wonderful [SelfieCamJams](https://youtu.be/9DbChOUotfo) and Benjamin
Gibbard's amazingly heartwarming
[Live from Home series](https://www.youtube.com/playlist?list=PLVuKHi9v2Rn6WytY_26KfgO2F2yp4Gqgv),
which now have their own entries on
[Setlist.fm](https://www.setlist.fm/search?artist=5bd69b98&query=tour:%28Live+from+Home%29).

There are more examples of humans being wonderful, you don't need me to
speak to all of them (especially with John Krasinski starting his
[Some Good News series](https://youtu.be/F5pgG1M_h_U)), so thank God for
the resilience of the Human Race. Stay true.

_-Nicholas_

[Back to Top](#notes)


## Joining the IndieWeb

_Posted 2020-03-30._

I've had a personal website of some form since I realized Drexel
University provided free hosting to their students way back in late 2012
or early 2013. I even still have a
[copy of the repo](https://github.com/nickkachur/academic_website) I put
up on Drexel Pages, dated 2013. I've long thought it to be important to
have my own curated presence on the web, but I hadn't heard of this idea
being distilled into the [IndieWeb Movement](https://indieweb.org/)
until just recently when I followed a link from
[HackerNews](https://news.ycombinator.com) about the importance of using
[static websites for emergency response](https://mxb.dev/blog/emergency-website-kit/).

I can't find the original HackerNews post in my history to show you, but
traveling down the rabbit hole of yet another personal website led me to
proponents of the IndieWeb Movement, putting a face and a name to
something I implicitly felt strongly about. From my early reading, there
seem to be a lot of good ideas about how and why to successfully develop
your personal website (see
[Rage Against the Content Machine](https://noti.st/mxb/lhMFMv/rage-against-the-content-machine)
also by Max), so here's to joining the IndieWeb in full.

_-Nicholas_

[Back to Top](#notes)


## Rereading His Dark Materials

_Posted and edited 2020-03-30._

I don't remember when I first read the His Dark Materials Trilogy,
likely in my pre-teens, but I do remember enjoying it immensely, even if
I didn't understand all the subtleties at the time. Rereading it now
leaves me in awe of the fantastic world-building, the pacing, buildup of
dramatic tension, and foreshadowing. It feels like a masterful work, and
has ensnared me once again. I highly recommend.

_-Nicholas_

[Back to Top](#notes)


## Books I Remember Reading in 2019

_Posted 2020-03-29._

I've been thinking about challenges to read a book every week of the
year and recommended reading lists and book journals. I've felt for a
long time that I'm not reading a lot, definitely not as voraciously as
when I was young, so I decided to start a book log to measure my way
towards improvement.

In 2019 and in no particular order, I remember reading:

- [Maybe You Should Talk to Someone](https://lorigottlieb.com/books/maybe-you-should-talk-to-someone/)
  by Lori Gottlieb, which was a fun romp through a therapist's life and
  work. No big revelations here, but a fun, quick read.
- [Beginning to Pray](https://store.ancientfaith.com/beginning-to-pray-metropolitan-anthony-bloom/)
  by Metropolitan Anthony Bloom was an affecting introduction to prayer
  starting with sitting in silence and coming to feel a togetherness
  with God. Short, simple, and accessible spirituality for the Orthodox
  heart.
- [Grit](https://angeladuckworth.com/grit-book/) by Angela Duckworth, a
  straightforward Psychology treatise on the importance of sticking out
  hard times. Seems useful, but was light on how to build grit yourself.
- [An Unquiet Mind](https://en.wikipedia.org/wiki/An_Unquiet_Mind) by
  Kay Redfield Jamison, the classic memoir picked up for a reread after
  encountering in a local used bookstore.
- [Night Falls Fast](https://www.penguinrandomhouse.com/books/86627/night-falls-fast-by-kay-redfield-jamison/)
  also by Jamison on how to understand and prevent suicide. Deals with
  its topic clearly and with compassion. I don't know how helpful it
  would be to a mourner, but I found it to be prophylactic.

And that's it for this first microblog, not really micro, but we'll get
there. Here's to reading in 2020!

_-Nicholas_

[Back to Top](#notes)