---
home: true
heroImage: /profile.jpg
heroText: I am Nicholas Kachur.
tagline: Welcome to my home!
footer: 
  Except where otherwise noted, the content of this website is
  licensed under a Creative Commons Attribution 4.0 International
  license.
---

I am an early career technologist, an Orthodox Christian, and an amateur
friendly person.

I grew up in the great city of Pittsburgh, Pennsylvania in the United
States, before making the journey across the state to Philadelphia to
attend Drexel University, from which I graduated in 2018 with a degree
in Computer Engineering. I've been working since then at Susquehanna
International Group (SIG), a financial firm headquartered outside of
Philadelphia where I did two internships. In addition to SIG, I had the
pleasure to work at Apple for a six-month internship, where I made many
friends and enjoyed my first adventures in California.

Outside my interests in technology, I read, make music, drink tea, spend
time with friends, and sometimes play squash. I am a committed Orthodox
Christian and active participant in parish life, where I sing in the
choir and participate in various other ministries. In the end, I aim to
be a good person, no matter how many times I stumble in the attempt.

If you'd like to get in touch, you can email
[hello@kachur.me](mailto:hello@kachur.me).

Thank you for visiting, and have a wonderful day!

_-Nicholas_