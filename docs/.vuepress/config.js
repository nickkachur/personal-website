module.exports = {
  title: 'Kachur.me',
  description: 'Welcome to the online home of Nicholas Kachur.',
  dest: 'public',
  themeConfig: {
    search: false,
    lastUpdated: 'Last Updated',
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Notes', link: '/notes/' },
      { text: 'Writings', link: '/writings/' },
      { text: 'Experience', link: '/experience/' },
      { text: 'Projects', link: '/projects/' },
      { text: 'Contact', link: 'mailto:hello@kachur.me' }
    ]
  }
}
