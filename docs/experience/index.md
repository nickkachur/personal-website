# Experience

In my short time as a working adult, I've delivered medicine, sold books,
administered computers, and written software. I'm proud of the jobs I had when I
was younger, and grateful for the experiences I've had since.

I've developed myself into a warm and welcoming technologist with a
focus on the intersection between development and operations. My primary
language is Python, but I've also experienced the expressiveness of Perl
and Ruby, the power of C and C++, the excitement of Swift, and the
balance of C#. If any of this interests you, you can find contact
information on my [current standard resume](/resume.pdf).

# Work History

## Susquehanna International Group, LLP

### Application Support Software Engineer

_June 2020 &ndash; Present_

In my latest role at SIG, I write automated ready-for-business reports
and management tools for the Proprietary Trading Services Team, which
supports effective stock and derivatives trading. My projects to-date
have used Python 3 and C# .NET technology stacks.

### Test Automation Software Engineer

_January 2018 &ndash; June 2020_

In my previous role, I developed automated unit and integration tests
in IronPython against a large, .NET based, options trading platform. I
tested various functional systems including risk management for market
taking strategies and our next generation spot feed provider. I was also
responsible for investigating and escalating test failures, responding
to build system help requests, and feature development. In my last three
months in this role, I led virtual scrums for our team of twelve, and
started transitioning to a new team.

### Test Automation Software Engineering Co-op

_March 2016 &ndash; September 2016_

While a student, I worked on my current team as a full-time co-op, designing and
developing a new test code runner. I utilized existing internal libraries to
consolidate test running logic into an extensible package that is still being
used by the team as a critical piece of infrastructure to this day.

## Apple Inc.

### Linux and vSphere Systems Architecture Co-op

_September 2014 &ndash; March 2015_

As a third-year student, I worked full-time for Apple's Silicon Engineering
Group in Cupertino. I primarily spent my time with the team's vSphere
infrastructure, planning and testing an upgrade path from vSphere 5.1 to 5.5. In
doing so, I wrote ESXi Kickstart and PXE-Boot scripts to automate deployment,
and wrote scripts using the vSphere Perl SDK to aid in our planned downtime
event.

## Drexel University

### Assistant Linux Systems Administrator

_April 2014 &ndash; September 2014_

I worked part-time during my second year at Drexel as an assistant SysAdmin
in the Department of Computer Science, responding to student, faculty, and
staff support requests, and assisting with the Department's internal website
and configuration management infrastructure.
